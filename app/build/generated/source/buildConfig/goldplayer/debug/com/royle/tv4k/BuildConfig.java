/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.royle.tv4k;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.ilike.hd4k";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "goldplayer";
  public static final int VERSION_CODE = 5;
  public static final String VERSION_NAME = "1";
}
