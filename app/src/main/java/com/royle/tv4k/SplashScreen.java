package com.royle.tv4k;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import helper.DataStore;


/**
 * Created by Naifun
 */
public class SplashScreen extends Activity {

    private final static int    UNINSTALL_REQUEST_CODE = 0;
    private final static String m_sPackageToUninstall  = "com.royle.tv8k"; //old client to be uninstall

    private SharedPreferences appSettings;

    private final static String appname = "iLike4K";
    private final static String Shortcutname = "iLike4K";

    static final int DIALOG_ERROR_CONNECTION = 1;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);


        if (!isOnline(this)) {
            showDialog(DIALOG_ERROR_CONNECTION); //displaying the created dialog.
            super.onPause();
        } else {
            //Internet available. Do what's required when internet is available.






        appSettings = getSharedPreferences(appname, MODE_PRIVATE);
        // Make sure you only run addShortcut() once, not to create duplicate shortcuts.
        if(!appSettings.getBoolean("shortcut", false)) {
            addShortcut();
        }

        logout(SplashScreen.this);
        Intent intent = new Intent(SplashScreen.this,ServiceCheck.class);
        stopService(intent);

//        installplayer();
//        AISOnAirTV();
//        launchSplash();



        // Check for the previous Client and uninstall it
        try
        {
            PackageManager oPackageManager = getPackageManager();
            PackageInfo oPackageInfo = oPackageManager.getPackageInfo(m_sPackageToUninstall, PackageManager.GET_ACTIVITIES);
            if (oPackageInfo != null)
            {


                Uri oPackageUri = Uri.parse("package:" + m_sPackageToUninstall);
                Intent oIntent = new Intent(Intent.ACTION_DELETE, oPackageUri);
                // wait till the uninstall is not completed
                startActivityForResult(oIntent, UNINSTALL_REQUEST_CODE);
                SystemClock.sleep(8000);

                launchSplash();


            }
            else
            {
                //launchApp();
                launchSplash();
            }
        }
        catch (PackageManager.NameNotFoundException e)
        {
            //launchApp();
            launchSplash();
        }
    }
    }


    public boolean isOnline(Context c) {
        ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        if (ni != null && ni.isConnected())
            return true;
        else
            return false;
    }

    private void addShortcut() {
        //Adding shortcut for MainActivity
        //on Home screen
        Intent shortcutIntent = new Intent(getApplicationContext(), SplashScreen.class);
        shortcutIntent.setAction(Intent.ACTION_MAIN);

        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, Shortcutname);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.drawable.logo));
        addIntent.putExtra("duplicate", false);
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);

        SharedPreferences.Editor prefEditor = appSettings.edit();
        prefEditor.putBoolean("shortcut", true);
        prefEditor.commit();

        Toast.makeText(getApplicationContext(), "กำลังสร้าง... Shortcut", Toast.LENGTH_SHORT).show();

    }




    private void launchSplash()
    {



        // Launch the new client
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        TextView t = (TextView) findViewById(R.id.lin_lay);
        t.clearAnimation();
        t.startAnimation(anim);
        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        TextView ix = (TextView) findViewById(R.id.lin_lay);
        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ix.clearAnimation();
        ix.startAnimation(anim);

        Animation animx = AnimationUtils.loadAnimation(this, R.anim.alpha);
        animx.reset();
        ImageView lx = (ImageView) findViewById(R.id.logo);
        lx.clearAnimation();
        lx.startAnimation(animx);
        animx = AnimationUtils.loadAnimation(this, R.anim.translate);
        animx.reset();


        Thread timerThread = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    Intent intent = new Intent(SplashScreen.this, UpdateCheck.class);
                    startActivity(intent);
                }
            }
        };
        timerThread.start();

    }


    private void installplayer(){

        // TODO Auto-generated method stub
        try {
            if (playerInstalledOrNot("com.mxtech.videoplayer.gold")) {

                Toast.makeText(getApplicationContext(), "Player พร้อมใช้งาน", Toast.LENGTH_LONG).show();

            } else {
                installgold atualizaApp = new installgold();
                atualizaApp.setContext(getApplicationContext());
                atualizaApp.execute("GoldPlayer.apk");


            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
        }
    }

    private void AISOnAirTV(){

        // TODO Auto-generated method stub
        try {
            if (playerInstalledOrNot("com.ais.mimo.AISOnAirTV")) {

                Toast.makeText(getApplicationContext(), "Sport พร้อมใช้งาน", Toast.LENGTH_LONG).show();

            } else {
                installgold atualizaAppS = new installgold();
                atualizaAppS.setContext(getApplicationContext());
                atualizaAppS.execute("AISPLAYBOX.apk");
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
        }
    }



    private boolean playerInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }



    public class installgold extends AsyncTask<String, Void, Void> {
        private Context context;

        public void setContext(Context contextf) {
            context = contextf;
        }

        @Override
        protected Void doInBackground(String... arg0) {
            try {
                String nameapp = (String) arg0[0];
                String link = "http://4kmoviestar.com/New_Apk/" + nameapp;
                URL url = new URL(link);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.setDoOutput(true);
                c.connect();

                String PATH = "/mnt/sdcard/Download/";
                File file = new File(PATH);
                file.mkdirs();
                File outputFile = new File(file, nameapp);
                if (outputFile.exists()) {
                    outputFile.delete();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);

                InputStream is = c.getInputStream();

                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();

//                Intent intent = new Intent(Intent.ACTION_VIEW);
//                intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/" + nameapp)), "application/vnd.android.package-archive");
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
//                context.startActivity(intent);


                try
                {
                    Runtime.getRuntime().exec(new String[] {"su", "-c", "pm install -r /mnt/sdcard/Download/" + nameapp});
                }
                catch (IOException e)
                {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setDataAndType(Uri.fromFile(new File("/mnt/sdcard/Download/" + nameapp)), "application/vnd.android.package-archive");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK); // without this flag android returned a intent error!
                    context.startActivity(intent);
                }

            } catch (Exception e) {
                Log.e("UpdateAPP", "Update error! " + e.getMessage());
            }
            return null;
        }
    }

    public static void logout(Context context){
        DataStore dataStore = new DataStore(context);
        dataStore.SavedSharedPreference(DataStore.USER_ID, "");
        dataStore.SavedSharedPreference(DataStore.USER_NAME, "");
        dataStore.SavedSharedPreference(DataStore.PASSWORD, "");
        dataStore.SavedSharedPreference(DataStore.USER_LEVEL, "");
        dataStore.SavedSharedPreference(DataStore.USER_LEVEL_ID, "");
        dataStore.SavedSharedPreference(DataStore.USER_EXPIRE, "");
        dataStore.SavedSharedPreference(DataStore.USER_TOKEN, "");
        dataStore.ClearSharedPreference();
    }





    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }



    protected void onActivityResult(int nRequestCode, int nResultCode, Intent oIntent)
    {
        super.onActivityResult(nRequestCode, nResultCode, oIntent);

        if (nRequestCode == UNINSTALL_REQUEST_CODE)
        {
            launchSplash();
        }
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        Dialog dialog = null;
        switch (id) {
            case DIALOG_ERROR_CONNECTION:
                AlertDialog.Builder errorDialog = new AlertDialog.Builder(this);
                errorDialog.setTitle("ไม่ได้เชื่อมต่อ INTERNET");
                errorDialog.setMessage("กรุณาเชื่อมต่อ WIFI ก่อนแล้วเข้าแอพอีกครั้ง");
                errorDialog.setNeutralButton("ตกลง",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.dismiss();
                                startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
                            }
                        });

                AlertDialog errorAlert = errorDialog.create();
                return errorAlert;

            default:
                break;
        }
        return dialog;
    }




}
