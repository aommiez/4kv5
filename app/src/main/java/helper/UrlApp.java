package helper;

public class UrlApp {

	public static final String API = "http://uway.bear-tv.com/api/";
	//token
	public static final String CHECK_TOKEN = API+"token_check/";
	//user
	public static final String REGISTER = API+"register";
	public static final String LOGIN = API+"login";
	public static final String CHANGEPASS = API+"change_password";
	public static final String BANK_ACCOUNT = API+"bankaccount";
	public static final String TRANSFER = API+"reqtransfer";
	//movie
	public static final String MAIN_MOVIE = API+"main_category";
	public static final String CATEGORY_MOVIE = API+"category";
	public static final String CHECK_EXPIRED = API+"check_expired/";
	public static final String CHECK_MONEY = API+"check_truemoney/";
	public static final String LAST_MOVIE = API+"getlastmovie";
	//series
	public static final String MAIN_Series = API+"series_main_category";
	public static final String CATEGORY_Series = API+"series_sub_category";
	public static final String SEASON_Series = API+"series_season";
	public static final String Series = API+"series";
	//tv
	public static final String MAIN_TV = API+"iptv_main_category";
	public static final String CATEGORY_TV = API+"iptv_sub_category";
	//search
	public static final String SEARCH = API+"findmovie";
	//search
	//public static final String SEARCHSR = API+"findseries";
	//other
	public static final String OTHER = API+"other";
	//slide
	public static final String SLIDE = API+"slide";
	//ebook
	public static final String EBOOK_CATEGORY = API+"ebook_category";
	public static final String EBOOK_ID = API+"ebook";
	//ip
	public static final String GET_IP = API + "getip";
	public static final String OTP = API + "otp";
	
	
	
	
	
	
	

}
